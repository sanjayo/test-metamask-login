import { useEffect, useState } from 'react';
import './App.css';
import { Form, Button } from 'react-bootstrap'
import Web3 from 'web3';


import MetaMask from './images/metamask_logo.png';

import { 
  containerStyle,
  buttonStyle,
  rowStyle,
  inputStyle,
} from './styles/styles'

function App() {

  const [ walletAccount, setWalletAccount ] = useState('')
  const [ ethBalance, setEthBalance ] = useState(null)
  const [ form, setForm ] = useState({ sender: '', receiver: '', amount: '' })
    const [ selectedWallet, setSelectedWallet ] = useState('')
    const [ sendError, setSendError ] = useState(false)
    const [ receiveError, setReceiveError ] = useState(false)

  useEffect(() => {
    if(typeof window.ethereum !== 'undefined') {
        window.ethereum.on('accountsChanged', (accounts) => {
          console.log('Account changed: ', accounts[0])
          setWalletAccount(accounts[0])
        })
        window.ethereum.on('chainChanged', (chaindId) => {
          console.log('Chain ID changed: ', chaindId)
        })
    } else {
        alert('Need MetaMask Browser Extension')
    }
  }, [])

  useEffect(() => {
  }, [walletAccount])

const handleFormChange = (event) => {
    
    setForm({
        ...form,
        [event.target.name]: event.target.value
    })
}

const sendTransaction = async () => {

    // Check to see if the input addresses are valid ethereum addresses
    const checkSender = Web3.utils.isAddress(form.sender)
    const checkReceiver = Web3.utils.isAddress(form.receiver)

    if(!checkSender) setSendError(true)
    if(!checkReceiver) setReceiveError(true)

    // Check to make sure the Sender is selected in MetaMask wallet
    // Will cause error if Sender is not selected
    if(selectedWallet !== form.sender) {
      console.log(selectedWallet , form)
        alert('Please select the sender account in your MetaMask wallet!');
    }

    if(checkSender && checkReceiver && selectedWallet === form.sender) {
        setSendError(false)
        setReceiveError(false)
        handleSendTransaction(form.sender, form.receiver, form.amount)

    }
}


  const handleDisconnect = async () => {
      console.log('Logout From Metamask')
      setWalletAccount('')
      setEthBalance(0)
  }
  const handlePersonalSign = async () => {
    console.log('Sign Authentication')
    const nonce = Math.floor(Math.random() * 1000000);
    const message = [
      "Your Nonce is",
      nonce,
    ].join("\n\n")
    const accounts = await window.ethereum.request({ method: 'eth_requestAccounts' });
    const account = accounts[0];
    await window.ethereum.request({ method: 'personal_sign', params: [message, account] })
    setSelectedWallet(accounts[0])
    setForm({
      ...form,
      sender: accounts[0],
  })

    handleGetBalance()
  }
  const handleGetBalance = async () => {
    const accounts = await window.ethereum.request({ method: 'eth_requestAccounts' });
    const account = accounts[0];
    const balance  = await window.ethereum.request({ method: 'eth_getBalance' , params: [ account, 'latest' ]})
    const wei = parseInt(balance, 16)
    const gwei = (wei / Math.pow(10, 9)) // parse to Gwei
    const eth = (wei / Math.pow(10, 18))// parse to ETH
    setEthBalance({ wei, gwei, eth })
  }
 
  const handleSendTransaction = async (sender, receiver, amount) => {
    const gasPrice = '0x5208' // 21000 Gas Price
    const amountHex = (amount * Math.pow(10,18)).toString(16)
    const tx = {
      from: sender,
      to: receiver,
      value: amountHex,
      gas: gasPrice,
    }
    await window.ethereum.request({ method: 'eth_sendTransaction', params: [ tx ]})
  }

  return (
    <div className="App">
        <div className="container" style={containerStyle}>
          <img src={ MetaMask } style={{width:'50px'}} alt="MetaMask Logo"></img>
            <div className="row" style={rowStyle}>
              <div className="connect-button" onClick={handleDisconnect} style={buttonStyle}>
                  Logout
              </div>
            </div>

            <div className="row" style={rowStyle}>
              <div className="header-title" style={{marginBottom: '20px'}}>Login with Nonce (Signature)</div>
              <button className="connect-button" onClick={handlePersonalSign} style={buttonStyle}>
                  Login
              </button>
            </div>

            <div className="row" style={rowStyle}>
              <div className="header-title" style={{marginBottom: '20px'}}>Send ETH</div>
              <div style={{width: '100%', textAlign: 'center'}}>
            <Form.Control 
                isInvalid={sendError ? true : false} 
                value={form.sender} 
                onChange={handleFormChange} 
                name="sender" type="text" 
                placeholder="Sender's Address" 
                style={inputStyle} 
            />
            </div>
            <div>
            <Form.Control 
                isInvalid={receiveError ? true : false} 
                value={form.receiver}
                onChange={handleFormChange} 
                name="receiver" type="text" 
                placeholder="Receiving Address" 
                style={inputStyle} 
            />
            </div>
            <div>
            <Form.Control 
                value={form.amount}
                onChange={handleFormChange}
                name="amount" type="text" 
                placeholder="Amount in ETH" 
                style={inputStyle} 
            />
            </div>
            <div style={{marginTop: '40px', width: '100%', padding: '0px 20px'}}>
                <Button onClick={sendTransaction} size="sm" style={{width: '100px'}}>Send</Button>
            </div>

            </div>

            <div className="row" style={rowStyle}>
              <div className="header-title" style={{marginBottom: '20px'}}>Get Account Balance</div>
                  Balance ETH { ethBalance ? ethBalance.eth : 0 }
            </div>


        </div>
    </div>
  );
}

export default App;


